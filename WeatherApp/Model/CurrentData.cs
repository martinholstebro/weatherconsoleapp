﻿namespace WeatherApp
{
    public class CurrentData
    {
        public string Temperature { get; set; }
        public string SkyText { get; set; }
        public string Humidity { get; set; }
        public string WindText { get; set; }
    }

}
