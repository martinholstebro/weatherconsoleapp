﻿using RestSharp;
using System.Collections.Generic;
namespace WeatherApp.Service
{
    static class RestfulClient
    {
        public static string ReturnData(string baseUrl, Dictionary<string, string> parameters = null)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest(Method.GET);

            foreach (var parameter in parameters)
            {
                request.AddParameter(parameter.Key, parameter.Value);
            }

            IRestResponse response = client.Execute(request);
            var content = response.Content;
            return content;
        }
    }
}
