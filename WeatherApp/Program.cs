﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WeatherApp.Service;

namespace WeatherApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Martin's Vejr applikation startes... \n");

            while (true)
            {
                Start:
                Console.WriteLine("Indtast bynavn...");
                var city = Console.ReadLine();

                var parameters = new Dictionary<string, string>()
                {
                    {"location", city.ToString() },
                    {"degree", "C" }
                };

                string content = RestfulClient.ReturnData("http://vejr.eu/api.php", parameters);

                if (!content.Contains("false"))
                {

                    var data = MapWeatherDetail(content);

                    GenerateWeatherDetails(data);
                    Proceed();

                    goto Start;
                }

                Console.WriteLine("Indtast gyldigt bynavn!");
            }
        }

        private static void Proceed()
        {
            Console.WriteLine("\nIndtast \"exit\" for at afslutte eller tryk på vilkårlig tast for at fortsætte...\n");

            if (Console.ReadLine().Contains("exit", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("Vejr applikation afsluttes...");
                Environment.Exit(0);
            }
        }
        
        private static void GenerateWeatherDetails(WeatherDetail data)
        {
            if (data == null)
            {
                Console.WriteLine("Vi fandt ikke noget... Prøv igen!");
                return;
            }

            Console.WriteLine($"\nVejret i {data.LocationName} er \n" +
                                $"{data.CurrentData.Temperature} c° \n" +
                                $"{data.CurrentData.SkyText} \n" +
                                $"{data.CurrentData.Humidity} % luftfugtighed \n" +
                                $"{data.CurrentData.WindText}");
        }        

        private static WeatherDetail MapWeatherDetail(string data)
        {
            return JsonConvert.DeserializeObject<WeatherDetail>(data);
        }
    }

}
